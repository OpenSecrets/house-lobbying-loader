# House lobbying loader

A script to download, parse, and upload House lobbying disclosure data. This script takes a filing name from the House lobbying disclosure portal, parses the XML, and uploads the data to a MS SQL database.


## Prerequisites
- MSSQL database credentials
- `.env` file with variables set. See `.example.env` for required settings

## Usage

#### Getting a file name to input
The script takes a filename to target which dislosures to download. These can be found on

1. Visit the [House's disclosure portal](https://disclosurespreview.house.gov/)
2. Click the "Download" button in the top-right corner of the page
3. Check the "Download past filings in XML format" radio button
4. Note the name of the file you need from the dropdown, sans the date (e.g. `2021 4thQuarter XML`)

### Running the script
This command should be used through the Alfred bot by calling `@alfred run house-lobbying {FILENAME}`

#### Running the script locally
Run the script locally with the file name `python main.py 2021 4thQuarter XML`

##### With Docker

`docker build -t house-lobbying-loader . && docker run -t house-lobbying-loader`
