NAME   := house_lobbying_loader
TAG    := $(shell git log -1 --pretty=%h)
IMG    := ${NAME}:${TAG}
LATEST := ${NAME}:latest

export TAG

push:
	docker push ${NAME}

login:
	docker log -u ${DOCKER_USER} -p ${DOCKER_PASS}

stop:
	docker ps -a
	docker stop ${NAME}

run:
	@echo '************ BEFORE ************'
	docker ps -a
	docker image ls
	@echo '********************************'
	@echo ''

	FILENAME=$(FILENAME) docker-compose up

	docker-compose down --rmi all
