"""
Functions that handle interactions with the database.
"""
import os
import logging
import itertools

import pyodbc
from dotenv import load_dotenv


load_dotenv()


class DB:
    """
    Generic class to wrap database functions.
    """

    def __init__(self):
        self._conn = self._connect()
        self._cursor = self._conn.cursor()
        self._conn.autocommit = False

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, exception_traceback):
        self.close()

    def _connect(self):
        """
        Connect to the MS SQL server
        """
        try:
            logging.info("Connecting to database...")

            cxn_string = "DRIVER={{ODBC Driver 17 for SQL Server}};SERVER={0},{1};DATABASE={2};UID={3};PWD={4}".format(
                os.getenv("DB_HOST"),
                os.getenv("DB_PORT"),
                os.getenv("DB_DATABASE"),
                os.getenv("DB_USER"),
                os.getenv("DB_PASSWORD"),
            )
            return pyodbc.connect(cxn_string)

        except:
            logging.exception("Could not connect to database.")

    @property
    def connection(self):
        return self._conn

    def close(self):
        """
        Close the connection
        """
        if self.connection:
            self.connection.commit()
            self.connection.close()

    def execute(self, query):
        """
        Generic execution wrapper
        """
        try:
            with self._cursor as cursor:
                cursor.execute(query)
        except:
            logging.debug(query)
            logging.exception("Exception occurred while attempting query.")

    def insert_data(self, db_table, field_names, data):
        """
        Inserts records into a database table.

        Parameters
        ----------
        db_table : str
            The name of the table to insert data into.
        field_names : tuple
            A tuple of field names, in order, to use in the
            INSERT statement. Corresponds to data parameter.
        data : list
            A list of tuples to insert into the database.

        """

        sql_statement = "insert into dbo.{} ({}) values ({})".format(
            db_table,
            ", ".join(field_names),
            ", ".join(list(itertools.repeat("?", len(field_names)))),
        )
        try:
            with self._cursor as cursor:
                cursor.fast_executemany = True
                cursor.executemany(sql_statement, data)
                cursor.commit()

        except:
            logging.exception(
                "Exception occurred while inserting into the database.\n{0}\n{1}".format(
                    sql_statement, data
                )
            )

    def clear_tables(self):
        """
        Clear the neccessary House tables to prepare for new data.
        """
        query = """
            SET NOCOUNT ON;

            Delete from dbo.house_affiliated_orgs_raw
            Delete from dbo.house_agencies_raw
            Delete from dbo.house_foreign_entities_raw
            Delete from dbo.house_inactive_alis_raw
            Delete from dbo.house_inactive_foreign_entities_raw
            Delete from dbo.house_inactive_lobs_raw
            Delete from dbo.house_inactive_orgs_raw
            Delete from dbo.house_issues_raw
            Delete from dbo.house_issues_specific_raw
            Delete from dbo.house_lobby_raw
            Delete from dbo.house_lobbyists_raw
            Delete from dbo.house_spec_bills_raw
            Delete from dbo.house_updates_raw;"""

        try:
            with self._cursor as cursor:
                cursor.execute(query)
        except:
            logging.exception("Exception occurred while attempting to clear database.")
