import re


BILL_INDICATORS = {
    "HR": "H.R.",
    "HJRES": "H.J.RES",
    "SJRES": "S.J.RES",
    "JRES": "J.RES",
    "CONRES": "CON.RES",
    "AMDT": "AMDT",
    "SR": "S.R.",
    "CR": "C.R.",
    "SJ": "S.J.",
    "S": "S.",
    "S.": "S.",
    "H.R.": "H.R.",
    "RES": "RES.",
    "CON": "CON.",
    "HCONRES": "H.CON.RES",
    "SCONRES": "S.CON.RES",
    "SJ RES": "S.J.RES",
}


def extract_bill_numbers(issue_text, report_id, issue_id):

    # Remove periods from abbreviated words to simplify tokenization later
    abbreviations = re.findall(r"(?:[A-Z]\.)+", issue_text)
    for abb in abbreviations:
        issue_text = issue_text.replace(abb, abb.replace(".", ""))

    # Add space around troublesome chars
    spaced_chars = ["/", ";", "\n", "\\", "(", ")", ",", ".", ":"]
    for char in spaced_chars:
        issue_text = issue_text.replace(char, f" {char} ")

    # Remove extra punctuation
    issue_text = re.sub(r",|\.|-|–|uf0a7|#|;|:", "", issue_text)

    # Filter out extra spaces
    issue_text_parts = list(filter(None, issue_text.split(" ")))
    # print("------------------")
    # print("Text parts:", issue_text_parts, "\n")

    word_matches = []
    nonspaced_matches = []
    for idx, word in enumerate(issue_text_parts):

        # Check if the word is a non-spaced combination of letters and numbers such as HR201
        if bool(re.match("^(?=.*[0-9]$)(?=.*[a-zA-Z])", word)):
            letters = "".join(re.findall("[a-zA-Z]+", word))
            if letters in BILL_INDICATORS.keys():
                nonspaced_matches.append(word.upper().strip())

        # Otherwise, see if the word is a number or matches indicators in the list
        else:
            if word.upper().strip() in BILL_INDICATORS.keys():
                word_matches.append((idx, word))

            elif word.isdigit():
                word_matches.append((idx, word))

    bills = []

    if nonspaced_matches:
        for match in nonspaced_matches:
            re.split(r"(\d+)", match)
            bill_id = format_bill_id(match)
            bills.append(bill_id)

    if word_matches:

        prev_idx = 0
        current_word = ""
        compiled_words = []

        # Combine bill indicator words with bill numbers if they are neighbors
        for match_index, match_word in word_matches:

            # If word is made of letters, store it.
            if match_word.isalpha():
                current_word = " ".join([current_word, match_word])
                prev_idx = match_index

            # If word is made of numbers, append it to stored word and finish up the bill name.
            elif match_word.isdigit() and match_index == prev_idx + 1:
                if current_word != "":
                    current_word = " ".join([current_word, match_word])
                    bills.append(current_word.upper().strip())
                    compiled_words.append(current_word.upper())

                    # Reset stored word
                    current_word = ""

    # Format bill ids and return objs
    parsed_bills = []
    for bill in bills:
        bill_id = format_bill_id(bill)
        if bill_id:
            parsed_bills.append(
                {"issue_id": issue_id, "report_id": report_id, "bill_id": bill_id}
            )
    return parsed_bills


def format_bill_id(bill_name):
    try:
        bill_name = str(bill_name)
        bill_name_split = re.split(r"(\d+)", bill_name)
        bill_type = bill_name_split[0].strip()
        bill_num = bill_name_split[1]
        bill_type = BILL_INDICATORS[bill_type]

        return f"{bill_type}{bill_num}"

    except:
        # print("Error with", bill_name)
        return None
