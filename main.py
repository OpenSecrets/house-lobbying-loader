import os
import sys
import glob
import shutil
import logging
from io import BytesIO
from zipfile import ZipFile
from datetime import datetime
import xml.etree.ElementTree as ET

import requests
from urllib3.exceptions import InsecureRequestWarning

from utils.db import DB
from utils.bills import extract_bill_numbers


# House site throws SSL cert errors, so disable that warning output
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

logging.basicConfig(
    level=logging.DEBUG,
    filename="lobbying.log",
    format="%(asctime)s - %(levelname)s | %(message)s",
    datefmt="%d-%b-%y %H:%M:%S",
    filemode="w",
)


RUN_DATE = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
db = DB()


def download_xml(search_term):

    s = requests.Session()

    # # Initial session GET
    # endpoint = "https://clerkapi.house.gov/Elastic/search"
    # options_payload = {
    #     "Access-Control-Request-Method": "POST",
    #     "Origin": "https://disclosurespreview.house.gov",
    # }
    # s.options(endpoint, headers=options_payload, verify=False)

    # Get list of available zip files
    past_filings_endpoint = (
        "https://disclosurespreview.house.gov/data/LD/LdSearchPastFilings.json"
    )
    p = s.get(past_filings_endpoint, verify=False)

    # Search for the needed file
    dir_name = None
    for file_info in p.json():
        if search_term.lower() in file_info["name"].lower():
            print(f"Downloading {file_info['name']}")

            xml_zip_url = (
                f"https://disclosurespreview.house.gov/data/LD/{file_info['file']}"
            )
            zip_file = s.get(xml_zip_url)

            # Download and extract zip contents
            dir_name = file_info["file"].replace(".zip", "")
            ZipFile(BytesIO(zip_file.content)).extractall(dir_name)

    # If a file match was found, parse each file.
    if dir_name:
        for xml_file in glob.glob(f"{dir_name}/*.xml"):
            report_json = parse_report(xml_file)

            if report_json:
                upload_data(report_json)
            os.remove(f"{xml_file}")
        shutil.rmtree(dir_name)

    # If no matching file was found, log it.
    else:
        logging.warning(f"No report matching {search_term} found.")


def parse_report(xml_file):

    # Extract report id from filenames
    report_id = xml_file.split("/")[1].replace(".xml", "")
    logging.info("Parsing report #{}".format(report_id))

    # Specify report root element

    try:
        root = ET.parse(xml_file).getroot()
    except ET.ParseError:
        print("-- Error with file id", report_id)
        logging.warning(f"Error with file id: {report_id}")
        return None

    # Store parsed report data
    parsed_report = {
        "house_lobby_raw": {"report_id": report_id, "run_date": RUN_DATE},
        "house_issues_raw": [],
        "house_issues_specific_raw": [],
        "house_spec_bills_raw": [],
        "house_agencies_raw": [],
        "house_lobbyists_raw": [],
        "house_updates_raw": [],
        "house_foreign_entities_raw": [],
        "house_inactive_lobs_raw": [],
        "house_inactive_alis_raw": [],
        "house_affiliated_orgs_raw": [],
        "house_inactive_orgs_raw": [],
        "house_inactive_foreign_entities_raw": [],
    }

    # Get report's top-level data
    report_fields = (
        "imported",
        "pages",
        "submitURL",
        "organizationName",
        "prefix",
        "firstName",
        "lastName",
        "registrantDifferentAddress",
        "address1",
        "address2",
        "city",
        "state",
        "zip",
        "zipext",
        "country",
        "principal_city",
        "principal_state",
        "principal_zip",
        "principal_zipext",
        "principal_country",
        "selfSelect",
        "clientName",
        "clientGovtEntity",
        "senateID",
        "houseID",
        "reportYear",
        "reportType",
        "terminationDate",
        "noLobbying",
        "income",
        "expenses",
        "expensesMethod",
        "printedName",
        "signedDate",
    )

    for fieldname in report_fields:
        try:
            fieldname_text = (root.find(fieldname).text or "").strip()
        except AttributeError:
            fieldname_text = ""

        parsed_report["house_lobby_raw"].update({fieldname: fieldname_text})

    # Parse alis
    for issue_id, ali in enumerate(root.find("alis").findall("ali_info")):
        issue_id = issue_id + 1
        parsed_ali = parse_fields(ali, ["issueAreaCode", "foreign_entity_issues"])
        if parsed_ali:
            parsed_ali.update(
                {
                    "issue_id": str(issue_id),
                    "run_date": RUN_DATE,
                    "report_id": report_id,
                }
            )
            parsed_report["house_issues_raw"].append(parsed_ali)

        # Parse specific issues
        si = ali.find("specific_issues").find("description")
        if si.text and si.text.strip() != "":
            parsed_report["house_spec_bills_raw"] = extract_bill_numbers(
                si.text, report_id, issue_id
            )
            parsed_report["house_issues_specific_raw"].append(
                {
                    "issue_id": str(issue_id),
                    "run_date": RUN_DATE,
                    "report_id": report_id,
                    "specific_issue": si.text.strip(),
                }
            )

        # Parse federal agencies
        agencies = ali.find("federal_agencies")
        if agencies.text:
            agency_list = agencies.text.split(",")
            for agency in filter(None, agency_list):
                parsed_report["house_agencies_raw"].append(
                    {
                        "issue_id": str(issue_id),
                        "run_date": RUN_DATE,
                        "report_id": report_id,
                        "agency": agency.strip(),
                    }
                )

        # Parse lobbyists
        lobbyist_fields = [
            "lobbyistFirstName",
            "lobbyistLastName",
            "lobbyistSuffix",
            "coveredPosition",
            "lobbyistNew",
        ]
        for lobbyist in ali.find("lobbyists"):
            parsed_lobbyist = parse_fields(lobbyist, lobbyist_fields)
            if parsed_lobbyist:
                if parsed_lobbyist["lobbyistFirstName"] != "":
                    parsed_lobbyist.update(
                        {
                            "issue_id": str(issue_id),
                            "run_date": RUN_DATE,
                            "report_id": report_id,
                        }
                    )
                    parsed_report["house_lobbyists_raw"].append(parsed_lobbyist)

    # Parse updates
    updates_fields = (
        "clientAddress",
        "clientCity",
        "clientState",
        "clientZip",
        "clientZipext",
        "clientCountry",
        "prinClientCity",
        "prinClientState",
        "prinClientZip",
        "prinClientZipext",
        "prinClientCountry",
        "generalDescription",
        "affiliatedUrl",
    )
    updates_node = root.find("updates")
    updates = parse_fields(updates_node, updates_fields)

    if updates:
        parsed_report["house_updates_raw"].append(
            updates.update(
                {
                    "run_date": RUN_DATE,
                    "report_id": report_id,
                }
            )
        )

    # Parse update's child elements
    updates_subsections = [
        {
            "name": "inactive_lobbyists",
            "table": "house_inactive_lobs_raw",
            "fields": ["firstName", "lastName", "suffix"],
        },
        {
            "name": "affiliatedOrgs",
            "table": "house_affiliated_orgs_raw",
            "fields": [
                "affiliatedOrgName",
                "affiliatedOrgAddress",
                "affiliatedOrgCity",
                "affiliatedOrgState",
                "affiliatedOrgZip",
                "affiliatedOrgCountry",
                "affiliatedPrinOrgCity",
                "affiliatedPrinOrgState",
                "affiliatedPrinOrgCountry",
            ],
        },
        {
            "name": "foreignEntities",
            "table": "house_foreign_entities_raw",
            "fields": [
                "name",
                "address",
                "city",
                "state",
                "country",
                "prinCity",
                "prinState",
                "prinCountry",
                "contribution",
                "ownership_Percentage",
            ],
        },
    ]

    for section in updates_subsections:
        section_node = updates_node.find(section["name"])
        for entry in section_node:
            section_data = parse_fields(entry, section["fields"])
            if section_data:
                section_data.update(
                    {
                        "run_date": RUN_DATE,
                        "report_id": report_id,
                    }
                )
                parsed_report[section["table"]].append(section_data)

    # Parse inactive ALIs
    inactive_alis = updates_node.find("inactive_ALIs")
    for iali in inactive_alis:
        if iali.text and iali.text.strip() != "":
            parsed_report["house_inactive_alis_raw"].append(
                {
                    "run_date": RUN_DATE,
                    "report_id": report_id,
                    "ali_code": iali.text.strip(),
                }
            )

    # Parse inactive orgs
    inactive_orgs = updates_node.find("inactiveOrgs")
    for iorg in inactive_orgs:
        if iorg.text and iorg.text.strip() != "":
            parsed_report["house_inactive_orgs_raw"].append(
                {
                    "run_date": RUN_DATE,
                    "report_id": report_id,
                    "inactiveOrgName": iorg.text.strip(),
                }
            )

    # Parse inactive foreign entities
    inactive_orgs = updates_node.find("inactive_ForeignEntities")
    for ife in inactive_orgs:
        if ife.text and ife.text.strip() != "":
            parsed_report["house_inactive_orgs_raw"].append(
                {
                    "run_date": RUN_DATE,
                    "report_id": report_id,
                    "inactive_ForeignEntity": ife.text.strip(),
                }
            )

    return parsed_report


def upload_data(report_json):
    # db = DB()
    for table, data in report_json.items():
        if table == "house_lobby_raw":
            logging.info(
                "insert into {} ({}) values ({})".format(
                    table,
                    ",".join(data.keys()),
                    ",".join(f'"{v}"' for v in data.values()),
                )
            )

            db.insert_data(table, tuple(data.keys()), [tuple(data.values())])

        else:
            if data:
                for item in data:
                    if item:
                        db.insert_data(
                            table, tuple(item.keys()), [tuple(item.values())]
                        )

        db._conn.commit()


def parse_fields(node, mapping_fields):
    data = {}
    for field in mapping_fields:
        f_value = node.find(field)
        if f_value.text:
            data[field] = f_value.text.strip()
    if any([val for val in data.values()]):
        return data
    else:
        return None


if __name__ == "__main__":

    # Set requests filename arg
    filename = os.environ.get("FILENAME")

    # If the arg is 'clear-tables', it will clear out existing data
    if filename == "clear-tables":
        print("Clearing out _raw tables...")
        logging.info("Clearing out _raw tables...")
        db.clear_tables()

    # Otherwise, fetch and load the requested file
    elif filename:
        print(f"Getting {filename}")
        download_xml(filename)

    else:
        print("Filename not found")
