FROM opensecrets/python-310-mssql:latest

ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    POETRY_VERSION=1.0.5


WORKDIR /app
COPY . /app/

RUN pip3 install "poetry==$POETRY_VERSION"
# RUN poetry config virtualenvs.create false
RUN poetry install --no-dev --no-interaction --no-ansi


CMD [ "poetry", "run", "python", "main.py" ]

# ENTRYPOINT ["tail", "-f", "/dev/null"]
