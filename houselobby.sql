ALTER LOGIN sa ENABLE ;  
GO

CREATE DATABASE LobbyHouse
GO

USE [LobbyHouse]
GO
/****** Object:  Table [dbo].[__house_lobbyists_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__house_lobbyists_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[lobbyistFirstName] [varchar](50) NULL,
	[lobbyistLastName] [varchar](50) NULL,
	[lobbyistSuffix] [varchar](50) NULL,
	[coveredPosition] [varchar](100) NULL,
	[lobbyistNew] [char](3) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[check_houselob1]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_houselob1]
(
	[lobbyist_id] [varchar](12) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[master_lobbyistid] [char](12) NULL,
	[master_lobbyistnew] [varchar](50) NULL,
	[mark] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[check_inactivelobbyists]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_inactivelobbyists]
(
	[report_id] [varchar](20) NULL,
	[reportyear] [char](4) NULL,
	[type] [varchar](4) NULL,
	[firstname] [varchar](30) NULL,
	[lastname] [varchar](50) NULL,
	[suffix] [varchar](10) NULL,
	[newclient] [varchar](50) NULL,
	[newregistrant] [varchar](50) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[lobbyist] [varchar](50) NULL,
	[lobbyistprefix] [char](10) NULL,
	[lobbyistfirstname] [varchar](50) NULL,
	[lobbyistmiddle] [varchar](20) NULL,
	[lobbyistlastname] [varchar](50) NULL,
	[lobbyistsuffix] [varchar](50) NULL,
	[lobbyistrest] [varchar](50) NULL,
	[mark] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[check_lobbyist_diferrentRegistrant]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_lobbyist_diferrentRegistrant]
(
	[lobbyist_id] [varchar](12) NULL,
	[lobbyist] [varchar](50) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[newregistrant] [varchar](50) NULL,
	[newregistrant_newdata] [varchar](50) NULL,
	[maxyear] [varchar](50) NULL,
	[minyear] [varchar](50) NULL,
	[mark] [varchar](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[check_lobbyists_employment]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_lobbyists_employment]
(
	[newregistrant] [varchar](50) NULL,
	[minyear] [varchar](50) NULL,
	[maxyear] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[lobbyist] [varchar](50) NULL,
	[mark] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[check_lobbyists_parse]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_lobbyists_parse]
(
	[report_id] [varchar](20) NULL,
	[reportyear] [char](4) NULL,
	[type] [varchar](4) NULL,
	[firstname] [varchar](30) NULL,
	[lastname] [varchar](50) NULL,
	[suffix] [varchar](10) NULL,
	[newclient] [varchar](50) NULL,
	[newregistrant] [varchar](50) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[lobbyist] [varchar](50) NULL,
	[lobbyistprefix] [char](10) NULL,
	[lobbyistfirstname] [varchar](50) NULL,
	[lobbyistmiddle] [varchar](20) NULL,
	[lobbyistlastname] [varchar](50) NULL,
	[lobbyistsuffix] [varchar](50) NULL,
	[lobbyistrest] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[check_lobbyists_sameregistrant]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_lobbyists_sameregistrant]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[lobbyist] [varchar](50) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[newregistrant] [varchar](50) NULL,
	[newregistrant_newdata] [varchar](50) NULL,
	[maxyear] [varchar](50) NULL,
	[mark] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[check_lobbyists_sameregistrantlast3f]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_lobbyists_sameregistrantlast3f]
(
	[lobbyist] [varchar](50) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[mark] [varchar](1) NOT NULL,
	[lobbyist_id] [varchar](12) NULL,
	[first] [varchar](25) NULL,
	[last] [varchar](25) NULL,
	[middle] [varchar](20) NULL,
	[prefix] [char](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[check_nulluse]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_nulluse]
(
	[report_id] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[check_orgname_conflict]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_orgname_conflict]
(
	[year] [char](4) NULL,
	[client] [varchar](110) NULL,
	[SenClient] [varchar](50) NULL,
	[SenUltorg] [varchar](50) NULL,
	[reportyear] [char](4) NULL,
	[clientname] [varchar](100) NULL,
	[newclient] [varchar](50) NULL,
	[ultorg] [varchar](50) NULL,
	[mark] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[compare_lobbyist]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[compare_lobbyist]
(
	[ID] [int] NOT NULL,
	[Field1] [nvarchar](255) NULL,
	[Field2] [nvarchar](255) NULL,
	[Field3] [int] NULL,
	[mark] [char](1) NULL,
	[reccount] [int] NULL,
	[lobbyist_id] [char](12) NULL,
	[maxyear] [char](4) NULL,
	[minyear] [char](4) NULL,
	[maxreg] [varchar](50) NULL,
	[minreg] [varchar](50) NULL,
	[maxregnew] [varchar](50) NULL,
	[minregnew] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[compare_lobbyorgs]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[compare_lobbyorgs]
(
	[tempclient] [varchar](150) NULL,
	[newclient] [varchar](50) NULL,
	[score_compare] [int] NULL,
	[score_diff] [int] NULL,
	[mark] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[coveredposition]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[coveredposition]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[lobbyist] [varchar](50) NULL,
	[coveredposition] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_affiliated_orgs_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_affiliated_orgs_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[affiliatedOrgName] [varchar](200) NULL,
	[affiliatedOrgAddress] [varchar](100) NULL,
	[affiliatedOrgCity] [varchar](50) NULL,
	[affiliatedOrgState] [varchar](5) NULL,
	[affiliatedOrgZip] [varchar](10) NULL,
	[affiliatedOrgCountry] [varchar](20) NULL,
	[affiliatedPrinOrgCity] [varchar](50) NULL,
	[affiliatedPrinOrgState] [varchar](5) NULL,
	[affiliatedPrinOrgCountry] [varchar](20) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_affiliates_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_affiliates_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[affiliatedOrgName] [varchar](200) NULL,
	[affiliatedOrgAddress] [varchar](100) NULL,
	[affiliatedOrgCity] [varchar](50) NULL,
	[affiliatedOrgState] [varchar](5) NULL,
	[affiliatedOrgZip] [varchar](10) NULL,
	[affiliatedOrgCountry] [varchar](20) NULL,
	[affiliatedPrinOrgCity] [varchar](50) NULL,
	[affiliatedPrinOrgState] [varchar](5) NULL,
	[affiliatedPrinOrgCountry] [varchar](20) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_agencies_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_agencies_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[agency] [varchar](250) NULL,
	[run_date] [varchar](20) NULL,
	[agencyid] [char](4) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_agencies_all_bu]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_agencies_all_bu]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[agency] [varchar](125) NULL,
	[run_date] [varchar](20) NULL,
	[agencyid] [char](4) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_agencies_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_agencies_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[agency] [varchar](250) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_foreign_entities_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_foreign_entities_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[name] [varchar](200) NULL,
	[address] [varchar](100) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](20) NULL,
	[country] [varchar](20) NULL,
	[prinCity] [varchar](50) NULL,
	[prinState] [varchar](20) NULL,
	[prinCountry] [varchar](20) NULL,
	[contribution] [varchar](20) NULL,
	[ownership_Percentage] [varchar](20) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_foreign_entities_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_foreign_entities_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[name] [varchar](200) NULL,
	[address] [varchar](100) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](20) NULL,
	[country] [varchar](20) NULL,
	[prinCity] [varchar](50) NULL,
	[prinState] [varchar](20) NULL,
	[prinCountry] [varchar](20) NULL,
	[contribution] [varchar](20) NULL,
	[ownership_Percentage] [varchar](20) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_inactive_alis_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_inactive_alis_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[ali_code] [varchar](5) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_inactive_foreign_entities_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_inactive_foreign_entities_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[inactive_ForeignEntity] [varchar](100) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_inactive_foreign_entities_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_inactive_foreign_entities_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[inactive_ForeignEntity] [varchar](100) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_inactive_lobs_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_inactive_lobs_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[firstName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[suffix] [varchar](50) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_inactive_lobs_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_inactive_lobs_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[firstName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[suffix] [varchar](50) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_inactive_lobs_work]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_inactive_lobs_work]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[In_Indiv] [char](1) NULL,
	[report_id] [varchar](20) NULL,
	[coveredPosition] [varchar](100) NULL,
	[Indicator] [char](11) NULL,
	[lobbyist] [varchar](50) NULL,
	[year] [varchar](50) NULL,
	[date] [varchar](50) NULL,
	[lobbyistprefix] [char](10) NULL,
	[lobbyistfirstname] [varchar](50) NULL,
	[lobbyistsuffix] [varchar](50) NULL,
	[lobbyistrest] [varchar](50) NULL,
	[lobbyistlastname] [varchar](50) NULL,
	[lobbyistmiddle] [varchar](20) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[rawfirstname] [varchar](50) NULL,
	[rawsuffix] [varchar](50) NULL,
	[rawlastname] [varchar](50) NULL,
	[cid] [varchar](9) NULL,
	[formercongmem] [char](1) NULL,
	[Congressno] [char](5) NULL,
	[memcycle] [char](10) NULL,
	[rev_id] [int] NULL,
	[mostrecentprof] [varchar](15) NULL,
	[date_processed] [smalldatetime] NULL,
	[mark] [char](1) NULL,
	[checked] [char](4) NULL,
	[newregistrant] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[lobbyist_id_temp] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_inactive_orgs_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_inactive_orgs_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[inactiveOrgName] [varchar](50) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_inactive_orgs_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_inactive_orgs_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[inactiveOrgName] [varchar](50) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_issues_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_issues_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[issueAreaCode] [varchar](5) NULL,
	[foreign_entity_issues] [varchar](2000) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_issues_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_issues_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[issueAreaCode] [varchar](5) NULL,
	[foreign_entity_issues] [varchar](2000) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_issues_specific_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_issues_specific_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [char](3) NULL,
	[specific_issue] [varchar](max) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_issues_specific_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_issues_specific_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [char](3) NULL,
	[specific_issue] [varchar](max) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_lobby_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_lobby_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[imported] [char](3) NULL,
	[pages] [char](3) NULL,
	[submitURL] [varchar](100) NULL,
	[organizationName] [varchar](100) NULL,
	[prefix] [varchar](10) NULL,
	[firstName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[registrantDifferentAddress] [varchar](5) NULL,
	[address1] [varchar](75) NULL,
	[address2] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [char](2) NULL,
	[zip] [varchar](10) NULL,
	[zipext] [varchar](4) NULL,
	[country] [varchar](20) NULL,
	[principal_city] [varchar](75) NULL,
	[principal_state] [varchar](20) NULL,
	[principal_zip] [varchar](10) NULL,
	[principal_zipext] [varchar](4) NULL,
	[principal_country] [varchar](20) NULL,
	[selfSelect] [varchar](20) NULL,
	[clientName] [varchar](100) NULL,
	[clientGovtEntity] [char](3) NULL,
	[senateID] [varchar](20) NULL,
	[houseID] [varchar](20) NULL,
	[reportYear] [varchar](4) NULL,
	[reportType] [varchar](5) NULL,
	[terminationDate] [varchar](20) NULL,
	[noLobbying] [varchar](20) NULL,
	[income] [varchar](20) NULL,
	[expenses] [varchar](20) NULL,
	[expensesMethod] [varchar](20) NULL,
	[printedName] [varchar](100) NULL,
	[signedDate] [varchar](30) NULL,
	[run_date] [varchar](20) NULL,
	[date_processed] [smalldatetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_lobby_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_lobby_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[imported] [char](3) NULL,
	[pages] [char](3) NULL,
	[submitURL] [varchar](100) NULL,
	[organizationName] [varchar](100) NULL,
	[prefix] [varchar](10) NULL,
	[firstName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[registrantDifferentAddress] [varchar](5) NULL,
	[address1] [varchar](75) NULL,
	[address2] [varchar](250) NULL,
	[city] [varchar](50) NULL,
	[state] [char](2) NULL,
	[zip] [varchar](10) NULL,
	[zipext] [varchar](4) NULL,
	[country] [varchar](20) NULL,
	[principal_city] [varchar](75) NULL,
	[principal_state] [varchar](20) NULL,
	[principal_zip] [varchar](10) NULL,
	[principal_zipext] [varchar](4) NULL,
	[principal_country] [varchar](20) NULL,
	[selfSelect] [varchar](20) NULL,
	[clientName] [varchar](100) NULL,
	[clientGovtEntity] [char](3) NULL,
	[senateID] [varchar](20) NULL,
	[houseID] [varchar](20) NULL,
	[reportYear] [varchar](4) NULL,
	[reportType] [varchar](5) NULL,
	[terminationDate] [varchar](20) NULL,
	[noLobbying] [varchar](20) NULL,
	[income] [varchar](20) NULL,
	[expenses] [varchar](20) NULL,
	[expensesMethod] [varchar](20) NULL,
	[printedName] [varchar](150) NULL,
	[signedDate] [varchar](30) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_lobbying_work]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_lobbying_work]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[Registrantid] [int] NULL,
	[organizationname] [varchar](100) NULL,
	[tempregistrant] [varchar](100) NULL,
	[ClientID] [int] NULL,
	[clientname] [varchar](100) NULL,
	[ClientCountry] [varchar](20) NULL,
	[CountryCRP] [varchar](34) NULL,
	[ClientCity] [varchar](75) NULL,
	[Clientst] [char](2) NULL,
	[ClientState] [varchar](30) NULL,
	[tempclient] [varchar](100) NULL,
	[reporttype] [varchar](5) NULL,
	[type] [varchar](4) NULL,
	[Typelong] [varchar](50) NULL,
	[period] [varchar](50) NULL,
	[reportyear] [char](4) NULL,
	[signeddate] [smalldatetime] NULL,
	[newregistrant] [varchar](50) NULL,
	[OrgId_Reg] [varchar](10) NULL,
	[isfirm] [char](1) NULL,
	[CRP_Reg_ID] [int] NULL,
	[newclient] [varchar](50) NULL,
	[OrgId_Clnt] [varchar](10) NULL,
	[ultorg] [varchar](50) NULL,
	[amount] [float] NULL,
	[catcode] [char](5) NULL,
	[sourcecode] [char](5) NULL,
	[self] [char](1) NULL,
	[IncludeNSFS] [char](1) NULL,
	[use] [char](1) NULL,
	[ind] [char](1) NULL,
	[mark] [char](1) NULL,
	[affiliate] [char](1) NULL,
	[cmteid] [char](11) NULL,
	[OrgID] [char](10) NULL,
	[run_date] [smalldatetime] NULL,
	[bigorg] [char](1) NULL,
	[ClientGovtEntity] [char](3) NULL,
	[ClientID_Type] [char](3) NULL,
	[FirmID_Type] [char](3) NULL,
	[SelfFiler] [varchar](5) NULL,
	[SenateID] [varchar](20) NULL,
	[HouseID] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_lobbying_work_new]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_lobbying_work_new]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[Registrantid] [int] NULL,
	[organizationname] [varchar](150) NULL,
	[tempregistrant] [varchar](150) NULL,
	[ClientID] [int] NULL,
	[clientname] [varchar](150) NULL,
	[ClientCountry] [varchar](20) NULL,
	[CountryCRP] [varchar](34) NULL,
	[ClientCity] [varchar](75) NULL,
	[Clientst] [char](2) NULL,
	[ClientState] [varchar](30) NULL,
	[tempclient] [varchar](150) NULL,
	[reporttype] [varchar](5) NULL,
	[type] [varchar](4) NULL,
	[Typelong] [varchar](50) NULL,
	[period] [varchar](50) NULL,
	[reportyear] [char](4) NULL,
	[signeddate] [smalldatetime] NULL,
	[newregistrant] [varchar](50) NULL,
	[OrgId_Reg] [varchar](10) NULL,
	[isfirm] [char](1) NULL,
	[CRP_Reg_ID] [int] NULL,
	[newclient] [varchar](50) NULL,
	[OrgId_Clnt] [varchar](10) NULL,
	[ultorg] [varchar](50) NULL,
	[amount] [int] NULL,
	[catcode] [char](5) NULL,
	[sourcecode] [char](5) NULL,
	[self] [char](1) NULL,
	[IncludeNSFS] [char](1) NULL,
	[use] [char](1) NULL,
	[ind] [char](1) NULL,
	[mark] [char](1) NULL,
	[affiliate] [char](1) NULL,
	[cmteid] [char](11) NULL,
	[OrgID] [char](10) NULL,
	[run_date] [smalldatetime] NULL,
	[bigorg] [char](1) NULL,
	[ClientGovtEntity] [char](3) NULL,
	[ClientID_Type] [char](3) NULL,
	[FirmID_Type] [char](3) NULL,
	[SelfFiler] [varchar](5) NULL,
	[SenateID] [varchar](20) NULL,
	[HouseID] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_lobbyists_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_lobbyists_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[lobbyistFirstName] [varchar](50) NULL,
	[lobbyistLastName] [varchar](50) NULL,
	[lobbyistSuffix] [varchar](50) NULL,
	[coveredPosition] [varchar](max) NULL,
	[lobbyistNew] [char](3) NULL,
	[run_date] [varchar](20) NULL,
	[date_processed] [smalldatetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_lobbyists_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_lobbyists_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[lobbyistFirstName] [varchar](50) NULL,
	[lobbyistLastName] [varchar](50) NULL,
	[lobbyistSuffix] [varchar](50) NULL,
	[coveredPosition] [varchar](max) NULL,
	[lobbyistNew] [char](3) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_lobbyists_work]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_lobbyists_work]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[In_Indiv] [char](1) NULL,
	[report_id] [varchar](20) NULL,
	[coveredPosition] [varchar](100) NULL,
	[Indicator] [char](11) NULL,
	[lobbyist] [varchar](50) NULL,
	[year] [varchar](50) NULL,
	[date] [varchar](50) NULL,
	[lobbyistprefix] [char](10) NULL,
	[lobbyistfirstname] [varchar](50) NULL,
	[lobbyistsuffix] [varchar](50) NULL,
	[lobbyistrest] [varchar](50) NULL,
	[lobbyistlastname] [varchar](50) NULL,
	[lobbyistmiddle] [varchar](20) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[cid] [varchar](9) NULL,
	[formercongmem] [char](1) NULL,
	[Congressno] [char](5) NULL,
	[memcycle] [char](10) NULL,
	[rev_id] [int] NULL,
	[mostrecentprof] [varchar](15) NULL,
	[date_processed] [smalldatetime] NULL,
	[mark] [char](1) NULL,
	[checked] [char](4) NULL,
	[newregistrant] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_lobbyists_work_new]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_lobbyists_work_new]
(
	[ID] [int] IDENTITY(1575021,1) NOT NULL,
	[In_Indiv] [char](1) NULL,
	[report_id] [varchar](20) NULL,
	[coveredPosition] [varchar](100) NULL,
	[Indicator] [char](11) NULL,
	[lobbyist] [varchar](50) NULL,
	[year] [varchar](50) NULL,
	[date] [varchar](50) NULL,
	[lobbyistprefix] [char](10) NULL,
	[lobbyistfirstname] [varchar](50) NULL,
	[lobbyistsuffix] [varchar](50) NULL,
	[lobbyistrest] [varchar](50) NULL,
	[lobbyistlastname] [varchar](50) NULL,
	[lobbyistmiddle] [varchar](20) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[rawfirstname] [varchar](50) NULL,
	[rawsuffix] [varchar](50) NULL,
	[rawlastname] [varchar](50) NULL,
	[cid] [varchar](9) NULL,
	[formercongmem] [char](1) NULL,
	[Congressno] [char](5) NULL,
	[memcycle] [char](10) NULL,
	[rev_id] [int] NULL,
	[mostrecentprof] [varchar](15) NULL,
	[date_processed] [smalldatetime] NULL,
	[mark] [char](1) NULL,
	[checked] [char](4) NULL,
	[newregistrant] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[lobbyist_id_temp] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_spec_bills_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_spec_bills_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[bill_id] [varchar](20) NULL,
	[CongNo] [char](3) NULL,
	[year] [char](4) NULL,
	[issueareacode] [varchar](5) NULL,
	[b_id] [varchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_spec_bills_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_spec_bills_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [int] NULL,
	[bill_id] [varchar](20) NULL,
	[CongNo] [char](3) NULL,
	[year] [char](4) NULL,
	[issueareacode] [char](3) NULL,
	[b_id] [varchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_trashreports]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_trashreports]
(
	[report_id] [varchar](20) NULL,
	[organizationname] [varchar](100) NULL,
	[clientname] [varchar](100) NULL,
	[autoid] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_updates_all]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_updates_all]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[clientAddress] [varchar](150) NULL,
	[clientCity] [varchar](50) NULL,
	[clientState] [varchar](20) NULL,
	[clientZip] [varchar](10) NULL,
	[clientZipext] [varchar](4) NULL,
	[clientCountry] [varchar](30) NULL,
	[prinClientCity] [varchar](50) NULL,
	[prinClientState] [varchar](20) NULL,
	[prinClientZip] [varchar](10) NULL,
	[prinClientZipext] [varchar](4) NULL,
	[prinClientCountry] [varchar](30) NULL,
	[generalDescription] [varchar](350) NULL,
	[affiliatedUrl] [varchar](350) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[house_updates_raw]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[house_updates_raw]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_id] [varchar](20) NULL,
	[clientAddress] [varchar](150) NULL,
	[clientCity] [varchar](50) NULL,
	[clientState] [varchar](20) NULL,
	[clientZip] [varchar](10) NULL,
	[clientZipext] [varchar](4) NULL,
	[clientCountry] [varchar](30) NULL,
	[prinClientCity] [varchar](50) NULL,
	[prinClientState] [varchar](20) NULL,
	[prinClientZip] [varchar](10) NULL,
	[prinClientZipext] [varchar](4) NULL,
	[prinClientCountry] [varchar](30) NULL,
	[generalDescription] [varchar](350) NULL,
	[affiliatedUrl] [varchar](350) NULL,
	[run_date] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Houselobcompare]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Houselobcompare]
(
	[ID] [int] NOT NULL,
	[Field1] [nvarchar](255) NULL,
	[Field2] [nvarchar](255) NULL,
	[Field3] [int] NULL,
	[mark] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lob_new_bu]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lob_new_bu]
(
	[ID] [int] IDENTITY(553533,1) NOT NULL,
	[In_Indiv] [char](1) NULL,
	[report_id] [varchar](20) NULL,
	[coveredPosition] [varchar](100) NULL,
	[Indicator] [char](11) NULL,
	[lobbyist] [varchar](50) NULL,
	[year] [varchar](50) NULL,
	[date] [varchar](50) NULL,
	[lobbyistprefix] [char](10) NULL,
	[lobbyistfirstname] [varchar](50) NULL,
	[lobbyistsuffix] [varchar](50) NULL,
	[lobbyistrest] [varchar](50) NULL,
	[lobbyistlastname] [varchar](50) NULL,
	[lobbyistmiddle] [varchar](20) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[rawfirstname] [varchar](50) NULL,
	[rawsuffix] [varchar](50) NULL,
	[rawlastname] [varchar](50) NULL,
	[cid] [varchar](9) NULL,
	[formercongmem] [char](1) NULL,
	[Congressno] [char](5) NULL,
	[memcycle] [char](10) NULL,
	[rev_id] [int] NULL,
	[mostrecentprof] [varchar](15) NULL,
	[date_processed] [smalldatetime] NULL,
	[mark] [char](1) NULL,
	[checked] [char](4) NULL,
	[newregistrant] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[lobbyist_id_temp] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lobbying_orgnames]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lobbying_orgnames]
(
	[reportyear] [char](4) NULL,
	[report_id] [varchar](9) NULL,
	[clientname] [varchar](150) NULL,
	[organizationname] [varchar](150) NULL,
	[tempclient] [varchar](150) NULL,
	[tempregistrant] [varchar](150) NULL,
	[newclient] [varchar](50) NULL,
	[newregistrant] [varchar](50) NULL,
	[senateid] [varchar](20) NULL,
	[houseid] [varchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lobbyist_issues]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lobbyist_issues]
(
	[lobbyist] [varchar](50) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[lobbyistfirstname] [varchar](50) NULL,
	[lobbyistlastname] [varchar](50) NULL,
	[lobbyistsuffix] [varchar](50) NULL,
	[lobbyistnew_H] [char](3) NULL,
	[issueareacode] [varchar](5) NULL,
	[lobbyist_id_temp] [int] NULL,
	[year] [char](4) NULL,
	[auto_id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lobbyist_issues_new]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lobbyist_issues_new]
(
	[lobbyist] [varchar](50) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[report_id] [varchar](20) NULL,
	[issue_id] [varchar](20) NULL,
	[lobbyistfirstname] [varchar](50) NULL,
	[lobbyistlastname] [varchar](50) NULL,
	[lobbyistsuffix] [varchar](50) NULL,
	[lobbyistnew_H] [char](3) NULL,
	[issueareacode] [varchar](5) NULL,
	[lobbyist_id_temp] [int] NULL,
	[year] [char](4) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LobbyistIssueCounts]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LobbyistIssueCounts]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[lobbyist_new] [varchar](40) NULL,
	[ACC] [varchar](3) NULL,
	[ADV] [varchar](3) NULL,
	[AER] [varchar](3) NULL,
	[AGR] [varchar](3) NULL,
	[ALC] [varchar](3) NULL,
	[ANI] [varchar](3) NULL,
	[APP] [varchar](3) NULL,
	[ART] [varchar](3) NULL,
	[AUT] [varchar](3) NULL,
	[AVI] [varchar](3) NULL,
	[BAN] [varchar](3) NULL,
	[BEV] [varchar](3) NULL,
	[BNK] [varchar](3) NULL,
	[BUD] [varchar](3) NULL,
	[CAW] [varchar](3) NULL,
	[CDT] [varchar](3) NULL,
	[CHM] [varchar](3) NULL,
	[CIV] [varchar](3) NULL,
	[COM] [varchar](3) NULL,
	[CON] [varchar](3) NULL,
	[CPI] [varchar](3) NULL,
	[CPT] [varchar](3) NULL,
	[CSP] [varchar](3) NULL,
	[DEF] [varchar](3) NULL,
	[DIS] [varchar](3) NULL,
	[DOC] [varchar](3) NULL,
	[ECN] [varchar](3) NULL,
	[EDU] [varchar](3) NULL,
	[ENG] [varchar](3) NULL,
	[ENV] [varchar](3) NULL,
	[FAM] [varchar](3) NULL,
	[FIN] [varchar](3) NULL,
	[FIR] [varchar](3) NULL,
	[FOO] [varchar](3) NULL,
	[FOR] [varchar](3) NULL,
	[FUE] [varchar](3) NULL,
	[GAM] [varchar](3) NULL,
	[GOV] [varchar](3) NULL,
	[HCR] [varchar](3) NULL,
	[HOM] [varchar](3) NULL,
	[HOU] [varchar](3) NULL,
	[IMM] [varchar](3) NULL,
	[IND] [varchar](3) NULL,
	[INS] [varchar](3) NULL,
	[INT] [varchar](3) NULL,
	[LAW] [varchar](3) NULL,
	[LBR] [varchar](3) NULL,
	[MAN] [varchar](3) NULL,
	[MAR] [varchar](3) NULL,
	[MED] [varchar](3) NULL,
	[MIA] [varchar](3) NULL,
	[MMM] [varchar](3) NULL,
	[MON] [varchar](3) NULL,
	[NAT] [varchar](3) NULL,
	[PHA] [varchar](3) NULL,
	[POS] [varchar](3) NULL,
	[REL] [varchar](3) NULL,
	[RES] [varchar](3) NULL,
	[RET] [varchar](3) NULL,
	[ROD] [varchar](3) NULL,
	[RRR] [varchar](3) NULL,
	[SCI] [varchar](3) NULL,
	[SMB] [varchar](3) NULL,
	[SPO] [varchar](3) NULL,
	[TAR] [varchar](3) NULL,
	[TAX] [varchar](3) NULL,
	[TEC] [varchar](3) NULL,
	[TOB] [varchar](3) NULL,
	[TOR] [varchar](3) NULL,
	[TOU] [varchar](3) NULL,
	[TRA] [varchar](3) NULL,
	[TRD] [varchar](3) NULL,
	[TRU] [varchar](3) NULL,
	[UNM] [varchar](3) NULL,
	[URB] [varchar](3) NULL,
	[UTI] [varchar](3) NULL,
	[VET] [varchar](3) NULL,
	[WAS] [varchar](3) NULL,
	[WEL] [varchar](3) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lobbyistsnew]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lobbyistsnew]
(
	[lobbyist] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lobbyistsold]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lobbyistsold]
(
	[lobbyist] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[neworgs]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[neworgs]
(
	[tempclient] [varchar](110) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[oldorgs]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oldorgs]
(
	[newclient] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReportTypes]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportTypes]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[total] [int] NULL,
	[reporttype] [char](4) NULL,
	[type] [char](4) NULL,
	[typelong] [varchar](40) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[senate_inactive_lobs_removed]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[senate_inactive_lobs_removed]
(
	[ID] [int] NOT NULL,
	[In_Indiv] [char](1) NULL,
	[uniqidnew] [varchar](36) NULL,
	[OfficialPosition] [varchar](max) NULL,
	[Indicator] [char](11) NULL,
	[lobbyist] [varchar](50) NULL,
	[year] [varchar](50) NULL,
	[date] [varchar](50) NULL,
	[prefix] [char](10) NULL,
	[first] [varchar](50) NULL,
	[suffix] [varchar](10) NULL,
	[rest] [varchar](50) NULL,
	[last] [varchar](50) NULL,
	[middle] [varchar](50) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[cid] [varchar](15) NULL,
	[formercongmem] [char](1) NULL,
	[Congressno] [char](5) NULL,
	[memcycle] [char](10) NULL,
	[rev_id] [int] NULL,
	[mostrecentprof] [varchar](15) NULL,
	[date_processed] [smalldatetime] NULL,
	[mark] [char](1) NULL,
	[checked] [char](4) NULL,
	[newregistrant] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[report_id] [varchar](20) NULL,
	[newclient] [varchar](50) NULL,
	[type] [varchar](4) NULL,
	[lobbyist_senate_id] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[senate_inactive_lobs_removed2]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[senate_inactive_lobs_removed2]
(
	[ID] [int] NOT NULL,
	[In_Indiv] [char](1) NULL,
	[uniqidnew] [varchar](36) NULL,
	[OfficialPosition] [varchar](max) NULL,
	[Indicator] [char](11) NULL,
	[lobbyist] [varchar](50) NULL,
	[year] [varchar](50) NULL,
	[date] [varchar](50) NULL,
	[prefix] [char](10) NULL,
	[first] [varchar](50) NULL,
	[suffix] [varchar](10) NULL,
	[rest] [varchar](50) NULL,
	[last] [varchar](50) NULL,
	[middle] [varchar](50) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[cid] [varchar](15) NULL,
	[formercongmem] [char](1) NULL,
	[Congressno] [char](5) NULL,
	[memcycle] [char](10) NULL,
	[rev_id] [int] NULL,
	[mostrecentprof] [varchar](15) NULL,
	[date_processed] [smalldatetime] NULL,
	[mark] [char](1) NULL,
	[checked] [char](4) NULL,
	[newregistrant] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[lobbyist_senate_id] [bigint] NULL,
	[report_id] [varchar](20) NULL,
	[newclient] [varchar](50) NULL,
	[type] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[standardize_clients]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[standardize_clients]
(
	[tempclient] [varchar](110) NULL,
	[newclient] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[standardize_lobbyistNicks]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[standardize_lobbyistNicks]
(
	[lobbyist] [varchar](50) NULL,
	[lobbyistrest] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[standardize_registrants]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[standardize_registrants]
(
	[tempregistrant] [varchar](150) NULL,
	[newregistrant] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[temp_agency_stand]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_agency_stand]
(
	[agency] [varchar](80) NULL,
	[agency_short] [varchar](40) NULL,
	[agencyid] [varchar](4) NULL,
	[agency_code] [varchar](12) NULL,
	[inlob] [char](1) NULL,
	[inrev] [char](1) NULL,
	[autoid] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[temp_agency_stand_bu]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_agency_stand_bu]
(
	[agency] [varchar](250) NULL,
	[agencyid] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[temp_lobbyistswork]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_lobbyistswork]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[In_Indiv] [char](1) NULL,
	[report_id] [varchar](20) NULL,
	[coveredPosition] [varchar](100) NULL,
	[Indicator] [char](11) NULL,
	[lobbyist] [varchar](50) NULL,
	[year] [varchar](50) NULL,
	[date] [varchar](50) NULL,
	[lobbyistprefix] [char](10) NULL,
	[lobbyistfirstname] [varchar](50) NULL,
	[lobbyistsuffix] [varchar](50) NULL,
	[lobbyistrest] [varchar](50) NULL,
	[lobbyistlastname] [varchar](50) NULL,
	[lobbyistmiddle] [varchar](20) NULL,
	[lobbyistnew] [varchar](50) NULL,
	[cid] [varchar](9) NULL,
	[formercongmem] [char](1) NULL,
	[Congressno] [char](5) NULL,
	[memcycle] [char](10) NULL,
	[rev_id] [int] NULL,
	[mostrecentprof] [varchar](15) NULL,
	[date_processed] [smalldatetime] NULL,
	[mark] [char](1) NULL,
	[checked] [char](4) NULL,
	[newregistrant] [varchar](50) NULL,
	[lobbyist_id] [varchar](12) NULL,
	[lobbyist_id_temp] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[temp_revdooor_lobby_match]    Script Date: 10/21/2021 7:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_revdooor_lobby_match]
(
	[lobbyist_id] [varchar](12) NULL,
	[person_id] [int] NOT NULL,
	[lobbyistnew] [varchar](50) NULL,
	[crpname] [varchar](50) NULL,
	[mark] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
�